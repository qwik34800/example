Feature: Изменение параметров в ЛК

  Background: Start from the main page Atas
    Given Open first page of website "https://atas.net/"


  Scenario: Check for sets up changes in section Personal data
    When Authorized
    And Open settings of My profile
    And Change and deploy new First Name + Second Name + Date of Birth
    Then Compare the new Personal data objects on selected

    Scenario: Check for sets up changes in section Contact information
    When Authorized
    And Open settings of My profile
    And Change and deploy Country Сode + Phone Number
    Then Compare the new Contact information objects on selected

    Scenario: Check for sets up changes in section Address
    When Authorized
    And Open settings of My profile
    And Change and deploy Country + Region + Postal Code + Address
    Then Compare the new Address objects on selected