package gherkin.step;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPageStep {

    private final SelenideElement signInModal = $x("//*[@data-tabs-type='kt-tabs-login']");
    private final SelenideElement emailInput = $x("//*[@id='kt-tabs-login']//input[@type='email']");
    private final SelenideElement passwordInput = $x("//input[@type='password']");
    private final SelenideElement signInBtn = $x("//button[@id='form-btn-login']");
    private final String email = "wacon35939@octovie.com";
    private final String password = "!R6Q2&jKI";

    private final SelenideElement dashboardLink = $(By.linkText("MY ACCOUNT"));


    @When("Authorized")
    public void authorized() {
        signInModal.click();
        emailInput.setValue(email);
        passwordInput.sendKeys(password);
        signInBtn.click();
    }

    @And("Open settings of My profile")
    public void openDashboard() {
        dashboardLink.click();
    }
}
