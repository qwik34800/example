package gherkin.step;

import com.codeborne.selenide.Config;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.en.Given;

public class BeforeStep {

    @Given("Open first page of website {string}")
    public void openFirstPageOfWebsite(String url) {
        Configuration.browserPosition = "0x0";
        Configuration.browserSize = "1920x1080";
        Configuration.timeout = 10000;
        Selenide.open(url);
    }
}
