package gherkin.step;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;

import java.time.LocalDateTime;

public class AfterStep {

    @After
    public void tearDown(){
        WebDriverRunner.getWebDriver().close();
    }

    @io.cucumber.java.AfterStep
    public void afterEveryStep(Scenario scenario){
        if (scenario.isFailed()){
            Selenide.screenshot(LocalDateTime.now() + " " + scenario.getName());
        }
    }
}
