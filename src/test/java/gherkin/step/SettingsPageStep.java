package gherkin.step;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.*;

public class SettingsPageStep {

    //General

    private final ElementsCollection elementsOfOpenedCombobox = $$x(".//div[@class='dropdown-menu show']//*[@class='inner show']/ul/li");

    //Personal data (section)

    private final SelenideElement firstName = $x("//input[@name='firstName']");
    private final SelenideElement secondName = $x("//input[@name='lastName']");
    private final SelenideElement birthDateShow = $x("//input[@name='birthdate']");
    private final SelenideElement birthDateNextMonth = $("div.datepicker-days > table > thead > tr:nth-child(2) > th.prev");

    private String expectedBirthDate;


    //private String[] first =
    // new String[]{"M", "A", "V"};
    private final String first = "AAA";
    private final String second = "BBB";

    private final SelenideElement applyBtn = $x("//*[@id='kt_user_form1']//button[@type='submit']");

    //Contact information (section)

    private final SelenideElement countryCodeShow = $(".dropdown.bootstrap-select.form-control.input-phoneCode");
    private final SelenideElement phoneNumber = $x("//*[@id='kt_portlet_base_demo_2_1_tab_content']//input[@type='tel']");
    private final String phone = "701111111";

    //Address (section)

    private final SelenideElement countryShow = $(".dropdown.bootstrap-select.form-control.get-country.select001");
    private final SelenideElement city = $x("//input[@name='city']");
    private final String cityValue = "Giresun";
    private final SelenideElement zipCode = $x("//input[@name='zipCode']");
    private final String zipCodeValue = "12345";
    private final SelenideElement address = $x("//input[@name='address']");
    private final String addressValue = "Per";


    public String calcBirthDate(String expectedBirthDate){
        String prevBirthDate = birthDateShow.attr("value");
        String[] razdelenie = prevBirthDate.split("-");
        int monthint = Integer.parseInt(razdelenie[1]) - 1;
        String monthstr = Integer.toString(monthint);
        expectedBirthDate = String.join("-", razdelenie[0],"0" + monthstr, razdelenie[2]);
        return expectedBirthDate;
    }

    @And("Change and deploy new First Name + Second Name + Date of Birth")
    public void nameChanger() {
        firstName.setValue(first);
        secondName.setValue(second);
        birthDateShow.click();
        birthDateNextMonth.click();
        applyBtn.click();
        Selenide.refresh();

        //executeJavaScript(
        //        String.format("$('[name=\"%s\"]').val('%s')"), name, date
        //    );

    }

    @Then("Compare the new Personal data objects on selected")
    public void comparePersonalData() {
        String newFirst = firstName.getValue();
        Assertions.assertEquals(first, newFirst);

        String newSecond = secondName.getValue();
        Assertions.assertEquals(second, newSecond);


        String newBirthDate = birthDateShow.attr("value");
        Assertions.assertEquals(calcBirthDate(expectedBirthDate), newBirthDate); //не проверял
    }

    @And("Change and deploy Country Сode + Phone Number")
    public void phoneChanger() {
    countryCodeShow.click();
    elementsOfOpenedCombobox.get(1).click();
    phoneNumber.setValue(phone);
    applyBtn.click();
    }

    @Then("Compare the new Contact information objects on selected")
    public void compareContactInformation() {
        countryCodeShow.click();
        String countryCodeSelectedValue = elementsOfOpenedCombobox.get(1).getText(); //value of second elem
        String countryCodeActual = $(".dropdown.bootstrap-select.form-control.input-phoneCode").find("button").attr("title");
        Assertions.assertEquals(countryCodeSelectedValue, countryCodeActual);

        String newPhoneNumber = phoneNumber.getValue();
        Assertions.assertEquals(phone, newPhoneNumber);

    }

    @And("Change and deploy Country + Region + Postal Code + Address")
    public void addressChanger() {
        countryShow.click();
        elementsOfOpenedCombobox.get(2).click();
        city.setValue(cityValue);
        zipCode.setValue(zipCodeValue);
        address.setValue(addressValue);
        applyBtn.click();
    }

    @Then("Compare the new Address objects on selected")
    public void compareAddress() {
        countryShow.click();
        String countrySelectedValue = elementsOfOpenedCombobox.get(2).getText();
        String countryActual = $(".dropdown.bootstrap-select.form-control.get-country.select001").find("button").attr("title");
        System.out.println(countryActual);
        Assertions.assertEquals(countrySelectedValue, countryActual);

        String newCityValue = city.getValue();
        Assertions.assertEquals(cityValue, newCityValue);

        String newZipCodeValue = zipCode.getValue();
        Assertions.assertEquals(zipCodeValue, newZipCodeValue);

        String newAddressValue = address.getValue();
        Assertions.assertEquals(addressValue, newAddressValue);

    }
}
