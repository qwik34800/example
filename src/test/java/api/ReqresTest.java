package api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.*;

public class ReqresTest {

    private final static String URL = "https://reqres.in/";

    @Test
    public void checkAvatarAndIdTest(){

        Specs.installSpecification(Specs.requestSpec(URL), Specs.responseSpecOK200());

        //Вариант 1. Get-запрос
        List<UserData> users = given()
                .when()
                .get("api/users?page")
                .then().log().all()
                .extract().body().jsonPath().getList("data", UserData.class);

        //Вариант 1. Get-запрос без спецификации
        //List<UserData> users = given()
        //        .when()
        //        .contentType(ContentType.JSON) //тип данных
        //        .get(URL + "/api/users?page") //get post put delete - тут http-запросы
        //        .then().log().all()
        //        .extract().body().jsonPath().getList("data", UserData.class);
        //извлекаем . тело . элемент в теле находится по такому пути в json. извлекаем списком в конструктор юзер даты (path - узел json-а)

        //пример 1
        users.forEach(x-> Assertions.assertTrue(x.getAvatar().contains(x.getId().toString()))); //поскольку id числовое поле (int), преобразовываем в строку (string)

        //пример 2
        Assertions.assertTrue(users.stream().allMatch(x-> x.getEmail().endsWith("@reqres.in")));
        //Проверка . утверждаем истину,если . из потока запроса "всех пользователей" . получаем емейл каждого юзера  (allMatch) . в конце Email содержится @reqres.in (до последнего символа)
        //allMatch - если хотя бы одно из условий(одна строка из нескольких) возвращает false, тест падает

        //пример 3. как собрать отдельные элементы
        //пример 3.1. для String даты
        List <String> avatars = users.stream().map(UserData::getAvatar).collect(Collectors.toList());
        //создаем стринговый список avatars = из потока запроса . достаем только что-то одно (getAvatar) (стрим позволяет использовать метод из класса) . собираем и сразу помещаем в список list
        //Пример 3.2. для Int даты
        List <String> ids = users.stream().map(x -> x.getId().toString()).collect(Collectors.toList());
        //Структура немного иная: вызываем лямбду, собираем все Id и преобразовываем к String

        //Теперь нужная проверка (Assertion), чтобы перебрать и первый, и второй список
        //Это можно сделать банально циклом for
        for(int i = 0; i < avatars.size(); i++){
            Assertions.assertTrue(avatars.get(i).contains(ids.get(i)));
            //Берем какой-то элемент с индексом i из avatars и ids

        }
    }

    //Вариант 2. Post-запрос. Успешная регистрация
    @Test
    public void successRegTest(){
        Specs.installSpecification(Specs.requestSpec(URL), Specs.responseSpecOK200());

        //Тестовая дата ответа
        Integer id = 4;
        String token = "QpwL5tke4Pnpja7X4";

        //Тестовая дата запроса //Создаем экземпляр класса регистер с полями
        Register user = new Register("eve.holt@reqres.in", "pistol");

        SuccessRegister successRegister = given()
                .body(user) // тело запроса
                .when()
                .post("api/register") //куда нужно отправить
                .then().log().all()
                .extract().as(SuccessRegister.class);

        //Проверка на не пустой ответ. Сначала проверяем, что пришла хоть какая-то дата
        Assertions.assertNotNull(successRegister.getId());
        Assertions.assertNotNull(successRegister.getToken());

        Assertions.assertEquals(id, successRegister.getId()); // id - ожидаемый результат (объявлен выше), а актуальный getId получаем из экземпляра класса (то есть из ответа: дату извлекли в successRegister.class)
        Assertions.assertEquals(token, successRegister.getToken());
    }

    //Вариант 3. Post-запрос. Неудачная регистрация
    @Test
    public void unSuccessRegTest(){
        Specs.installSpecification(Specs.requestSpec(URL), Specs.responseSpecError400());

        //Тестовая дата запроса //Создаем экземпляр класса регистер с полями
        Register user = new Register("sydney@fife", "");
        UnSuccessRegister unSuccessReg = given()
                .body(user)
                .post("api/register")
                .then().log().all()
                .extract().as(UnSuccessRegister.class);
        Assertions.assertEquals("Missing password", unSuccessReg.getError());

    }

    //Вариант 4. Get-запрос. Нужно убедиться, что запросы отсортированы по годам
    @Test
    public void sortedYearsTest(){
        Specs.installSpecification(Specs.requestSpec(URL), Specs.responseSpecOK200());

        //В запросе приходит json, вытягиваем из json секцию data и, для каждой записи data, создается экземпляр класса в ColorsData
        List<ColorsData> colors = given()
                .when()
                .get("api/unknown")
                .then().log().all()
                .extract().body().jsonPath().getList("data",ColorsData.class);

        //Из полученных экземпляров, с различными полями, получаем одно единственное поле Year
        //В первозданном виде, как оно пришло из запроса
        List<Integer> years = colors.stream().map(ColorsData::getYear).collect(Collectors.toList());

        //Вручную сортируем список years
        List<Integer> sortedYears = years.stream().sorted().collect(Collectors.toList());

        //Проверяем ожидаемое с реальным
        Assertions.assertEquals(sortedYears, years);

        System.out.println(years);
        System.out.println(sortedYears);
    }

    //Вариант 5. Delete-запрос. Нужно удалить пользователя и (ТОЛЬКО) убедиться в корректном статус-коде
    @Test
    public void deleteUserTest(){
        Specs.installSpecification(Specs.requestSpec(URL), Specs.responseSpecUnique(204));
       given()
                .when()
                .delete("api/users/2")
                .then().log().all();

        //int stat = get().getStatusCode();

        //Assertions.assertTrue(Specs.responseSpecUnique(204), stat);

    }
}
